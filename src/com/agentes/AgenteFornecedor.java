

package com.agentes;

import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import static java.lang.Integer.parseInt;

import java.util.*;

public class AgenteFornecedor extends Agent {
	// guarda os livros a serem vendidos
	private Hashtable catalogo;

	// inicializa o agente
        @Override
	protected void setup() {
		// cria o catalogo
                System.out.println("Bem vindo agente fornecedor "+getAID().getName()+" tenha boas vendas!!!.");
		catalogo = new Hashtable();
                
                //  Adicionar produtos ao catálogo
                String[][] produtos = (String[][]) getArguments();

                int i;
                for(i = 0; i <= produtos.length; i ++)
                {
                    AtualizaCatalogo((String) produtos[0][i], (Integer)parseInt(produtos[1][i]));
                }

		// registra o servico do fornecedor nas PAGINAS AMARELAS
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("fornecer-produtos");
		sd.setName("Carteira JADE");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}

		// adiciona o comportamento para servir as consultas dos compradores
		addBehaviour(new OfertaProduto());

		// adiciona o comportamento servindo ordens de compra dos compradores
		addBehaviour(new ComprarProduto());
        }

	// finaliza o missao do agente
        @Override
	protected void takeDown() {
		// deleta o registro das PAGINAS AMARELAS
		try {
			DFService.deregister(this);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		// demite o agente
		System.out.println("Agente Fornecedor "+getAID().getName()+" Sua missao termina por aqui!!!");
	}

	/**
            e chamado cada vez que o fornecedor disponibiliza um novo produto
	 */
	public void AtualizaCatalogo(final String produto, final int preco) {
		addBehaviour(new OneShotBehaviour() {
			public void action() {
				catalogo.put(produto, new Integer(preco));
				System.out.println("Produto: "+produto+" esta disponivel pelo preço de R$ "+preco);
			}
		} );
	}

        /**
           * Se o produto solicitado está na listo do agente fornecedor, ele responde 
           * com uma mensagem PROPOSE especificando o preço, 
           * Caso contrário, uma mensagem de REFUSE é enviado de volta.
	 */
	private class OfertaProduto extends CyclicBehaviour {//este é um comportamento ciclico
                @Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				// processar a mensagem recebida
				String titulo = msg.getContent();
				ACLMessage resposta = msg.createReply();

				Integer preco = (Integer) catalogo.get(titulo);
				if (preco != null) {
					// se o produto estiver disponivel responde com o preco
					resposta.setPerformative(ACLMessage.PROPOSE);
					resposta.setContent(String.valueOf(preco.intValue()));
				}
				else {
					// o produto solicitado nao esta disponivel para venda
					resposta.setPerformative(ACLMessage.REFUSE);
					resposta.setContent("Nao disponivel");
				}
				myAgent.send(resposta);
			}
			else {
				block();
			}
		}
	}  //fim da classe

	/**
           * O agente fornecedor remove o produto de seu catálogo e 
           * responde com uma mensagem de INFORM para notificar o 
           * comprador de que a compra foi concluída com êxito.
	 */
	private class ComprarProduto extends CyclicBehaviour {
                @Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				// processa a mensagem
				String produto = msg.getContent();
				ACLMessage resposta = msg.createReply();

				Integer preco = (Integer) catalogo.remove(produto);
				if (preco != null) {
					resposta.setPerformative(ACLMessage.INFORM);
					System.out.println("O produto "+produto+" foi vendido ao agente "+msg.getSender().getName());
				}
				else {
					// o produto solicitado ja foi vendido
					resposta.setPerformative(ACLMessage.FAILURE);
					resposta.setContent("nao-disponivel");
				}
				myAgent.send(resposta);
			}
			else {
				block();
			}
		}
	}  //fim da classe
}
