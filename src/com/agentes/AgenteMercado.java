/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agentes;

import static com.agentes.Simulador.addAgentByArray;
import static com.agentes.Simulador.containerController;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import static java.lang.Integer.parseInt;
/**
 *
 * @author Samsung
 */
public class AgenteMercado extends Agent {
    
    protected void setup(){
        //Dispara mensagem de boas vindas
        System.out.println("Eu sou o agente mercado " + getAID().getName() + " e aqui você pode fazer suas compras");
        
        System.out.println("Estou contradando os fornecedores...");
        
        String[] produtosMercado = new String[1];
        produtosMercado[0] = "Tinta";
        
        //Cria fornecedores
        String[] fornecedores = new String[2];
        fornecedores [0] = "Guilherme";
        fornecedores [1] = "Gustavo";
        
        //Produtos a venda
        String[][] produtosForn1 = { {"Tinta impressora", "Pente de Memoria", "Cabo de rede"}, {"15", "120", "12", "8" }};

        String[][] produtosForn2 = { {"Tinta impressora", "Pente de Memoria", "Cabo de rede"}, {"20", "110", "10", "11"}};
        
        //Criar fornecedores
        Object[] args = getArguments();
        
        for(int coluna = 0; coluna < 3; coluna++)
        {
            if(coluna == 0)
            {
                ContratarFornecedor(fornecedores[coluna], produtosForn1);
            }
            else if(coluna == 1)
            {
                ContratarFornecedor(fornecedores[coluna], produtosForn2);
            }
        }
    }

    public void ContratarFornecedor(final String fornecedor, String[][] produtos) {
            addBehaviour(new OneShotBehaviour() {
                    public void action() {
                        addAgentByArray(containerController, fornecedor, AgenteFornecedor.class.getName(), produtos);
                    }
            } );
    }
    
    protected void takeDown() {
        // demite o agente
        System.out.println("Agente Mercado"+getAID().getName()+" Sua missao termina por aqui!!!");
    }
}
