package com.agentes;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class AgenteComprador extends Agent {
    //produto a ser comprado

    private String ProdutoComprar;
    // lista de fornecedores encontrados
    private AID[] AgentesFornecedores;

    // inicialização do agente inteligente
    @Override
    protected void setup() {
        // mensagem de boas vindas ao comprador
        System.out.println("_Olá! Agente-Comprador "
                + getAID().getName() + ", qual produto vc procura?");

        // pega o produto para fazer uma busca 
        Object[] args = getArguments();
        if (args != null && args.length > 0) {
            ProdutoComprar = (String) args[0];
            System.out.println("_Estou a procura do produto: "
                    + ProdutoComprar + "!!!");

            // adiciona um comportamento: programar o comprador para verificar um fornecedor a cada minuto
            addBehaviour(new TickerBehaviour(this, 30000) {
                @Override
                protected void onTick() {
                    System.out.println("Ola fornecedor, quero comprar  "
                            + ProdutoComprar);
                    // atualiza a lista de vendedores
                    DFAgentDescription template
                            = new DFAgentDescription();
                    ServiceDescription sd
                            = new ServiceDescription();
                    sd.setType("fornecer-produtos");
                    template.addServices(sd);
                    try {
                        DFAgentDescription[] result = DFService.search(myAgent, template);
                        System.out.println("Os seguintes agentes são fornecedores:");
                        AgentesFornecedores = new AID[result.length];
                        for (int i = 0; i < result.length; ++i) {
                            AgentesFornecedores[i] = result[i].getName();
                            System.out.println(AgentesFornecedores[i].getName());
                        }
                    } catch (FIPAException fe) {
                        fe.printStackTrace();
                    }

                    // executa o pedido
                    myAgent.addBehaviour(new PedidoCompra());
                }
            });
        } else {
            // termina a missao do agente
            System.out.println("O produto nao esta disponivel!");
            doDelete();
        }
    }

    // Put agent clean-up operations here
    @Override
    protected void takeDown() {
        // Printout a dismissal message
        System.out.println("Agente Comprador " + getAID().getName()
                + " obrigado, volte sempre!!!.");
    }

    /**
     * comportamento dos compradores.
     */
    private class PedidoCompra extends Behaviour {

        private AID bestSeller; // verif. o fornecedor com melhor oferta 
        private int bestPrice;  // melhor preco
        private int repliesCnt = 0; // conta as respostas dos fornecedor
        private MessageTemplate mt; // modelo de receber respostas
        private int step = 0;

        @Override
        public void action() {
            switch (step) {
                case 0:
                    // envia sua identificacao para tds os fornecedor
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i = 0; i < AgentesFornecedores.length; ++i) {
                        cfp.addReceiver(AgentesFornecedores[i]);
                    }
                    cfp.setContent(ProdutoComprar);
                    cfp.setConversationId("comercio-suprimentos");
                    cfp.setReplyWith("cfp" + System.currentTimeMillis()); // valor unico
                    myAgent.send(cfp);
                    // Prepara o modelo de receber respostas
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("comercio-suprimentos"),
                            MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                case 1:
                    // recebe tdas as respostas / recusado por agentes fornecedores
                    ACLMessage reply = myAgent.receive(mt);// MENSAGEM NO FORMATO ACL
                    if (reply != null) {
                        // responde
                        if (reply.getPerformative() == ACLMessage.PROPOSE) {
                            // oferta 
                            int price = Integer.parseInt(reply.getContent());
                            if (bestSeller == null || price < bestPrice) {
                                // este é o melhor fornecedor do momento
                                bestPrice = price;
                                bestSeller = reply.getSender();
                            }
                        }
                        repliesCnt++;//incrementa a oferta
                        if (repliesCnt >= AgentesFornecedores.length) {
                            // quando receber tds as ofertas
                            step = 2;
                        }
                    } else {
                        block();
                    }
                    break;
                case 2:
                    // Envie o pedido de compra para melhor fornecedor
                    ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    order.addReceiver(bestSeller);
                    order.setContent(ProdutoComprar);
                    order.setConversationId("comercio-suprimentos");
                    order.setReplyWith("Orden" + System.currentTimeMillis());
                    myAgent.send(order);
                    // modelo para resposta de ordem de compra
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("comercio-suprimentos"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    step = 3;
                    break;
                case 3:
                    //Recebe a resposta da ordem de compra
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        // resposta da ordem de compra recebida
                        if (reply.getPerformative() == ACLMessage.INFORM) {
                            //compra bem socedida
                            System.out.println("Produto " + ProdutoComprar + " comprado de " + reply.getSender().getName());
                            System.out.println("Preco: R$ " + bestPrice);
                            myAgent.doDelete();
                        } else {
                            System.out.println("Desculpe mas o produto já foi vendido!");
                        }

                        step = 4;
                    } else {
                        block();
                    }
                    break;
            }
        }

        @Override
        public boolean done() {
            if (step == 2 && bestSeller == null) {
                System.out.println("Desculpe mas o produto " + ProdutoComprar + " não esta a venda");
            }
            return ((step == 2 && bestSeller == null) || step == 4);
        }
    }  // fim
}
